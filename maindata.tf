data "azurerm_resource_group" "terraform1" {
  name = "${var.name_resource_groupe1}"
}

data "azurerm_virtual_network" "terraform1" {
  name = "${var.name_virtual_network_terraform1}"
  resource_group_name = "${data.azurerm_resource_group.terraform1.name}"
}

data "azurerm_subnet" "terraform1" {
  name = "${var.name_subnet}"
  resource_group_name = "${data.azurerm_resource_group.terraform1.name}"
  virtual_network_name = "${data.azurerm_virtual_network.terraform1.name}"

}
data "azurerm_network_security_group" "terraform1" {
  name = "${var.name_security_group}"
  resource_group_name = "${data.azurerm_resource_group.terraform1.name}"
}
data "azurerm_storage_account" "newstorageaccountgtm" {
  name = "${var.name_storage_account}"
  resource_group_name = "${data.azurerm_resource_group.terraform1.name}"

}